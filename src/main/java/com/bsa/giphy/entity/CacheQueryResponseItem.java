package com.bsa.giphy.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class CacheQueryResponseItem {
    private String query;
    private Set<String> gifs;
}
