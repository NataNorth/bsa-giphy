package com.bsa.giphy.entity;

import lombok.Getter;

@Getter
public class HistoryResponseItem {
    private String date;
    private String query;
    private String path;
}
