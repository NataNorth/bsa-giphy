package com.bsa.giphy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.nio.file.Path;

@Data
@AllArgsConstructor
public class Gif {
    private String id;
    private String path;
    private String type;
}
