package com.bsa.giphy.service;

import com.bsa.giphy.dto.GenerateGifRequestDto;
import com.bsa.giphy.entity.CacheQueryResponseItem;
import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.repository.Cache;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class GifOperationService {

    private final Logger logger = LoggerFactory.getLogger(GifOperationService.class);

    private final Cache cache;

    public GifOperationService(Cache cache) {
        this.cache = cache;
    }

    public  static URL parseGifUrl(URL url) throws IOException {
        InputStream in = url.openStream();
        String text = null;
        try (Scanner scanner = new Scanner(in, StandardCharsets.UTF_8.name())) {
            text = scanner.useDelimiter("\\A").next();
        }
        Pattern pattern = Pattern.compile("https://i\\.giphy\\.com/media/\\w+/giphy\\.gif");
        Matcher matcher = pattern.matcher(text);
        String actualDownloadUrl = null;
        if (matcher.find()) {
            actualDownloadUrl = matcher.group(0);
            System.out.println(matcher.group(0));
        }
        return new URL(actualDownloadUrl);
    }

    public static void downloadFile(URL url, String outputFileName) throws IOException {
        InputStream in = url.openStream();
        ReadableByteChannel rbc = Channels.newChannel(in);
        FileOutputStream fos = new FileOutputStream(outputFileName);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
    }

    @Nullable
    private CacheQueryResponseItem createCacheQueryResponse(Map<String, String> query, File dir) {
        CacheQueryResponseItem responseItem = new CacheQueryResponseItem();
        if (query.isEmpty() || query.get("type").equals(dir.getName())) {
            responseItem.setQuery(dir.getName());
            Set<String> gifs = new HashSet<>();
            for (File gif : dir.listFiles()) {
                gifs.add(gif.getAbsolutePath());
            }
            responseItem.setGifs(gifs);
            return responseItem;
        } else {
            return null;
        }
    }

    public List<CacheQueryResponseItem> getGifsByQuery(Map<String,String> query, String path) {
        File cache = new File(path);
        List<CacheQueryResponseItem> cacheQueryResponse = new LinkedList<>();
        if (cache.isDirectory()) {
            for (File item : cache.listFiles()) {
                if (item != null){
                    CacheQueryResponseItem responseItem = createCacheQueryResponse(query, item);
                    if (responseItem != null)
                        cacheQueryResponse.add(responseItem);
                }

            }
        }
        return cacheQueryResponse;
    }

    private void createTypeDirectory(String path, String query){
        File cache = new File(path);
        File typeDir = null;
        for (File item : cache.listFiles()) {
            if (item.getName().equals(query)) {
                typeDir = new File(path + "/" + item.getName());
            }
        }
        if (typeDir == null) {
            typeDir = new File(path + "/" + query);
            boolean created = typeDir.mkdir();
            if (created) {
                logger.info(query + " folder has been created");
            }
        }
    }

    public void writeToHistory(String message, String pathToFile){

        Path path = Paths.get(pathToFile);
        byte[] strToBytes = message.getBytes();
        try {
            File file = new File(pathToFile);
            if(file.exists()){
                Files.write(path, strToBytes, StandardOpenOption.APPEND);
            } else {
                Files.write(path, "date,query,path\n".getBytes());
                Files.write(path, strToBytes, StandardOpenOption.APPEND);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public String getDate(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    public void createDirectory(String path) {
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdir();
    }

    private String findDownloadUrl(String text) {
        Pattern pattern = Pattern.compile("https:[^\"]+giphy\\.gif");
        Matcher matcher = pattern.matcher(text);
        String downloadUrl;
        if (matcher.find()){
            downloadUrl = matcher.group(0);
            System.out.println(matcher.group(0));
            downloadUrl = downloadUrl.replaceAll("\\\\", "");
            System.out.println(downloadUrl);
        } else {
            System.out.println("No match");
            downloadUrl = "";
        }
        return downloadUrl;
    }

    private String findGifId(String text) {
        Pattern pattern = Pattern.compile("\"id\":\"[^\"]+");
        Matcher matcher = pattern.matcher(text);
        String id = null;
        if (matcher.find())
        {
            id = matcher.group(0);
            System.out.println(matcher.group(0));
            id = id.substring(6); // "id":" omitted
            System.out.println(id);
        } else {
            System.out.println("No match");
//            id = "";
        }
        return id;
    }

    public Gif downloadGifFromApi(GenerateGifRequestDto generateGifRequestDto, GifsApiClient gifsApiClient, String path){
        createTypeDirectory(path, generateGifRequestDto.getQuery());

        String response = gifsApiClient.getGifsAsJsonString(generateGifRequestDto.getQuery());

        if (response.equals("")) {
            return null;
        }

        String downloadUrl = findDownloadUrl(response);
        String id = findGifId(response);

        URL url = null;
        try {
            url = new URL(downloadUrl);
        } catch (MalformedURLException e) {
            logger.error(e.getMessage() + " MalformedURLException");
        }

        createDirectory(path + "/" + generateGifRequestDto.getQuery());

        String pathToGif = path + "/"+ generateGifRequestDto.getQuery() + "/" + id + ".gif";
        try {
            downloadFile(parseGifUrl(url), pathToGif);
        } catch (IOException e) {
            logger.error(e.getMessage() + " IOException download file");
        }
        Gif responseGif = new Gif(id, pathToGif, generateGifRequestDto.getQuery());
        return responseGif;
    }

    public List<CacheQueryResponseItem> generateGifInCache(GenerateGifRequestDto generateGifRequestDto, GifsApiClient gifsApiClient, String path){
        this.downloadGifFromApi(generateGifRequestDto, gifsApiClient, path);

        Map<String,String> typeMap = new HashMap<>();
        typeMap.put("type", generateGifRequestDto.getQuery());
        return getGifsByQuery(typeMap, path);
    }

    public List<String> getAllGifsFromCache() {
        String path = System.getProperty("user.dir") + "/server/cache";
        List<String> allGifsPaths = new LinkedList<>();
        File cache = new File(path);
        for (File type : cache.listFiles()){
            for (File gif : type.listFiles()) {
                allGifsPaths.add(path + "/" + type.getName() + "/" + gif.getName());
            }
        }
        return allGifsPaths;
    }

    public void deleteCache(String path) {
        File cache = new File(path);
        for (File type : cache.listFiles()) {
            for (File gif : type.listFiles()) {
                gif.delete();
            }
            if(type.isDirectory()) { //doesn`t delete history
                type.delete();
            }
        }
    }

    public String randomGif(Set<String> gifs) {
        int item = new Random().nextInt(gifs.size());
        int i = 0;
        for(String gif : gifs)
        {
            if (i == item)
                return gif;
            i++;
        }
        return null;
    }

    public String getUserGifFromRAMCache(String id, String type){
        var typeToGifs = cache.getUserGifsById(id);
        if (typeToGifs == null) {
            return null;
        }
        var gifs = typeToGifs.get("type");
        String path = System.getProperty("user.dir") + "/server/users/" + id;

        if(gifs.isEmpty()) {
            var gifsOnRAM = this.getGifsByQuery(Map.of("type", type), path);
            cache.addGifsToUser(id, type, gifsOnRAM.get(0).getGifs());
        }
        return randomGif(gifs);
    }

    public String getUserGifFromCache(String path, Map<String, String> query) {
        var gifs = this.getGifsByQuery(query, path);
        if (gifs == null) {
            return null;
        }
        return randomGif(gifs.get(0).getGifs());
    }

    public void copyFile(String source, String destination) {
        try {
            Files.copy(Paths.get(source), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            logger.error(e.getMessage() + " IOException copy");
        }
    }

    public List<Map<?,?>> getUserHistory(String id){
        String path = System.getProperty("user.dir") + "/server/users/" + id;
        File input = new File(path + "/history.csv");
        try {
            CsvSchema csv = CsvSchema.emptySchema().withHeader();
            CsvMapper csvMapper = new CsvMapper();
            MappingIterator<Map<?, ?>> mappingIterator =  csvMapper.reader().forType(Map.class).with(csv).readValues(input);
            List<Map<?, ?>> list = mappingIterator.readAll();
            System.out.println(list);
            return list;
        } catch(Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public void deleteUserHistory(String id){
        String path = System.getProperty("user.dir") + "/server/users/" + id + "/history.csv";
        File history = new File(path);
        history.delete();
    }

    public void resetUserRAMCache(String id, Map<String, String> query) {
        String type;
        if(query.isEmpty()) {
            type = "";
        }else{
            type = query.get("type");
        }
        cache.deleteByQuery(id, type);
    }

    public String generateUserGif(String id, GenerateGifRequestDto generateGifRequestDto, GifsApiClient gifsApiClient){
        String path = System.getProperty("user.dir") + "/server/users/" + id;
        this.createDirectory(path);
        String response;
        Gif responseGif;
        if (generateGifRequestDto.getForce()){
            String destinationPath = System.getProperty("user.dir") + "/server/cache/" + generateGifRequestDto.getQuery();
            this.createDirectory(destinationPath);
            responseGif = this.downloadGifFromApi(generateGifRequestDto, gifsApiClient, path);
            String sourceGifName = responseGif.getPath();
            String destinationGifName = destinationPath + "/" + responseGif.getId() + ".gif";
            this.copyFile(sourceGifName, destinationGifName);
            cache.addGifsToUser(id, responseGif.getType(), Set.of(sourceGifName));
        } else {
            response = this.getUserGifFromRAMCache(id, generateGifRequestDto.getQuery());
            if (response != null) {
                return response;
            }
            response = this.getUserGifFromCache(path, Map.of("type", generateGifRequestDto.getQuery()));
            if (response != null) {
                return response;
            }
            responseGif = this.downloadGifFromApi(generateGifRequestDto, gifsApiClient, path);
        }
        response = responseGif.getPath();
        return response;
    }
}

