package com.bsa.giphy.service;

import java.util.Map;

public interface GifsApiClient {
    String getGifsAsJsonString(String query);
}
