package com.bsa.giphy.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.SQLOutput;
import java.util.Map;

@Component(value = "realGifApi")
public final class HttpGifsApiClient implements GifsApiClient {
    private static final Logger logger = LoggerFactory.getLogger(HttpGifsApiClient.class);

    @Value("${api.gifs-url}") // we can reference application.properties values directly with @Value
    private String gifsApiUrl; // kebab-case to camelCase automatically

    private final HttpClient client;

    @Autowired
    public HttpGifsApiClient(HttpClient client) {
        this.client = client;
    }

    @Override
    public String getGifsAsJsonString(String query) {
        try {
            String queryUrl = gifsApiUrl.concat(query);

            logger.info("Query: " + queryUrl);

            var response = client.send(buildGetRequest(queryUrl), HttpResponse.BodyHandlers.ofString());
            logger.info("Response body: " + response.body());

            return response.body();
        } catch (IOException | InterruptedException ex) {
            logger.error(ex.getMessage(), ex);

            return "";
        }
    }

    private HttpRequest buildGetRequest(String apiUrl) {
        return HttpRequest
                .newBuilder()
                .uri(URI.create(apiUrl))
                .GET()
                .build();
    }
}
