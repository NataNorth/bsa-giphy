package com.bsa.giphy.repository;

import com.bsa.giphy.entity.Gif;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.*;

@Component
public class Cache {
    private final Logger logger = LoggerFactory.getLogger(Cache.class);

    private Map<String, Map<String, Set<String>>> cache = new HashMap<>();

    public Map<String, Set<String>> getUserGifsById(String id) {
        if (cache.isEmpty()) {
            return null;
        }
        return cache.get(id);
    }

    public void addGifsToUser(String id, String type, Set<String> paths) { //
        if (!cache.containsKey(id)) {
            if (!cache.get(id).containsKey(type)) {
                cache.put(id, Map.of(type, paths));
            } else {
                cache.get(id).get(type).addAll(paths);
            }
        } else {
            logger.error("No user");
        }
    }

    public void deleteByQuery(String id, String type) { //Todo: write this function
        if (cache.containsKey(id)) {
            if (type.isEmpty()) {
                cache.remove(id);
            } else {
                cache.get(id).remove(type);
            }
        }
    }
}
