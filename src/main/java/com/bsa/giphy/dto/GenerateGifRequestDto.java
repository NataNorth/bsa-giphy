package com.bsa.giphy.dto;

import lombok.Getter;

@Getter
public class GenerateGifRequestDto {
    private String query;
    private Boolean force;
}
