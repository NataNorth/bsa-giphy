package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.nio.file.Path;

@AllArgsConstructor
@Getter
public class GifDto {
    private String id;
    private Path path;
    private String type;
    private String url;
}