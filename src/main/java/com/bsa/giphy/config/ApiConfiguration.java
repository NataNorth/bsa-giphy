package com.bsa.giphy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
//import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
//import org.springframework.security.web.server.SecurityWebFilterChain;
//import org.springframework.security.config.web.server.ServerHttpSecurity;

import java.net.http.HttpClient;

@Configuration
public class ApiConfiguration {
    @Bean
    @Scope(value = "prototype") // singleton by default
    public HttpClient httpClient() {
        return HttpClient.newHttpClient();
    }

}
