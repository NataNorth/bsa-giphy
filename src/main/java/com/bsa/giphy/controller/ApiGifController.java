package com.bsa.giphy.controller;

import com.bsa.giphy.dto.GenerateGifRequestDto;
import com.bsa.giphy.entity.CacheQueryResponseItem;
import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.service.GifOperationService;
import com.bsa.giphy.service.GifsApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.io.File;
import java.util.*;

@RestController
@Validated
public class ApiGifController {
    @Qualifier(value = "realGifApi")
    @Autowired
    private GifsApiClient gifsApiClient;

    @Value("${spring.resources.static-locations}")
    private String ServerUrl;

    private static final Logger logger = LoggerFactory.getLogger(ApiGifController.class);

    private final GifOperationService gifOperationService;

//    @ExceptionHandler(ConstraintViolationException.class)
//    public ResponseEntity<String> onValidationError(Exception ex) {
//        return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
//    }

    @Autowired
    public ApiGifController(GifOperationService gifOperationService) {
        this.gifOperationService = gifOperationService;
    }

    @ResponseBody
    @GetMapping("/api/cache")
    public ResponseEntity<List<CacheQueryResponseItem>> queryCacheCollection(@RequestParam Map<String, String> query,
                                                                             @RequestHeader(value = "X-BSA-GIPHY") @Valid
                                                                             @Pattern(regexp = "Academy") String apiKeyHeader){
        String path = System.getProperty("user.dir") + "/server/cache";
        var cacheQueryResponse = gifOperationService.getGifsByQuery(query, path);

        logger.info(String.format("Request GET /api/cache/%s", query));


        if (cacheQueryResponse.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(cacheQueryResponse);
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(cacheQueryResponse);
        }
    }

    @PostMapping("/api/cache/generate")
    public ResponseEntity<List<CacheQueryResponseItem>> generateGif
            (@RequestBody GenerateGifRequestDto generateGifRequestDto, @RequestHeader(value = "X-BSA-GIPHY") @Valid
            @Pattern(regexp = "Academy") String apiKeyHeader) {
        String path = System.getProperty("user.dir") + "/server/cache";
        var cacheQueryResponse = gifOperationService.generateGifInCache(generateGifRequestDto, gifsApiClient, path);

        logger.info("Generate gif in cache");

        if (cacheQueryResponse.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(cacheQueryResponse);
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(cacheQueryResponse);
        }
    }

    @GetMapping("/api/gifs")
    public ResponseEntity<List<String>> getAllGifs(@RequestHeader(value = "X-BSA-GIPHY") @Valid
                                                       @Pattern(regexp = "Academy") String apiKeyHeader) {
        var allGifsPaths = gifOperationService.getAllGifsFromCache();

        logger.info("Get all gifs");

        if (allGifsPaths.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(allGifsPaths);
        }
    }

    @DeleteMapping("/api/cache")
    public void deleteCache(@RequestHeader(value = "X-BSA-GIPHY") @Valid
                                @Pattern(regexp = "Academy") String apiKeyHeader){
        String path = System.getProperty("user.dir") + "/server/cache";
        this.gifOperationService.deleteCache(path);

        logger.info("Cache was deleted");
    }

    @GetMapping("/api/user/{id}/all")
    public ResponseEntity<List<CacheQueryResponseItem>> getAllUserGifs(@PathVariable String id, @RequestHeader(value = "X-BSA-GIPHY") @Valid
    @Pattern(regexp = "Academy") String apiKeyHeader){
        String path = System.getProperty("user.dir") + "/server/cache/" + id;
        var cacheQueryResponse = gifOperationService.getGifsByQuery(null, path);

        logger.info(String.format("Get all %s`s gifs", id));

        if (cacheQueryResponse.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(cacheQueryResponse);
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(cacheQueryResponse);
        }
    }

    @GetMapping("/api/user/{id}/search")
    public ResponseEntity<String> getUserGif (@RequestParam Map<String, String> query, @PathVariable String id, @RequestHeader(value = "X-BSA-GIPHY") @Valid
    @Pattern(regexp = "Academy") String apiKeyHeader) {
        String path = System.getProperty("user.dir") + "/server/cache/" + id;
        String response = null;
        if (query.containsKey("force")) {
            response = gifOperationService.getUserGifFromCache(path, query);
            //randomly choose one and return
        } else {
            response = gifOperationService.getUserGifFromRAMCache(id, query.get("type"));
        }

        logger.info(String.format("Search for %s`s gifs", id));

        if(response == null){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
        }
        if (response.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
    }

    @PostMapping("/api/user/{id}/generate")
    public ResponseEntity<String> generateUserGif(@PathVariable String id, @RequestBody GenerateGifRequestDto generateGifRequestDto, @RequestHeader(value = "X-BSA-GIPHY") @Valid
    @Pattern(regexp = "Academy") String apiKeyHeader) {
        String pathToHistory = System.getProperty("user.dir") + "/server/cache/" + id + "/history.csv";
        var response = gifOperationService.generateUserGif(id, generateGifRequestDto, gifsApiClient);
        gifOperationService.writeToHistory(String.format("%s,%s,%s\n", gifOperationService.getDate(),
                generateGifRequestDto.getQuery(), response), pathToHistory);

        logger.info(String.format("Generate %s`s gif", id));

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping("/api/user/{id}/history")
    public ResponseEntity<List<Map<?, ?>>> getUserHistory(@PathVariable String id, @RequestHeader(value = "X-BSA-GIPHY") @Valid
    @Pattern(regexp = "Academy") String apiKeyHeader){
        var response = gifOperationService.getUserHistory(id);

        logger.info(String.format("Show history of %s", id));

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @DeleteMapping("api/user/{id}/history/clean")
    public void cleanUserHistory(@PathVariable String id, @RequestHeader(value = "X-BSA-GIPHY") @Valid
    @Pattern(regexp = "Academy") String apiKeyHeader) {
        gifOperationService.deleteUserHistory(id);
        logger.info(String.format("History of %s was deleted", id));
    }

    @DeleteMapping("api/user/{id}/reset")
    public void resetUserCache(@PathVariable String id, @RequestParam Map<String, String> query, @RequestHeader(value = "X-BSA-GIPHY") @Valid
    @Pattern(regexp = "Academy") String apiKeyHeader){
        gifOperationService.resetUserRAMCache(id, query);

        logger.info(String.format("Cache of %s was deleted with %s query", id, query));
    }

    @DeleteMapping("api/user/{id}/clean")
    public void cleanUserCash(@PathVariable String id, @RequestHeader(value = "X-BSA-GIPHY") @Valid
    @Pattern(regexp = "Academy") String apiKeyHeader) {
        gifOperationService.resetUserRAMCache(id, new HashMap<>());
        String path = System.getProperty("user.dir") + "/server/users/" + id;
        gifOperationService.deleteCache(path);

        logger.info(String.format("Cache of %s was deleted", id));
    }
}
